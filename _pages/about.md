---
layout: about
title: About
permalink: /
subtitle: <a href='http://www.ict.cas.cn/'>Affiliations</a>. Institute of Computing Technology, Chinese Academy of Sciences

profile:
  align: right
  image: liyewen_2018.jpg
  image_circular: true # crops the image to make it circular
  # address: >
  #   <p>555 your office number</p>
  #   <p>123 your address street</p>
  #   <p>Your City, State 12345</p>

news: true  # includes a list of news items
selected_papers: true # includes a list of papers marked as "selected={true}"
social: true  # includes social icons at the bottom of the page
---

<!-- Write your biography here. Tell the world about yourself. Link to your favorite [subreddit](http://reddit.com). You can put a picture in, too. The code is already in, just name your picture `prof_pic.jpg` and put it in the `img/` folder.

Put your address / P.O. box / other info right below your picture. You can also disable any these elements by editing `profile` property of the YAML header of your `_pages/about.md`. Edit `_bibliography/papers.bib` and Jekyll will render your [publications page](/al-folio/publications/) automatically.

Link to your social media connections, too. This theme is set up to use [Font Awesome icons](http://fortawesome.github.io/Font-Awesome/) and [Academicons](https://jpswalsh.github.io/academicons/), like the ones below. Add your Facebook, Twitter, LinkedIn, Google Scholar, or just disable all of them. -->

I'm Yewen Li, a Ph.D. student at [Unique Research on Systems and Architecture(URSA) Group](https://ursa-ict.com/), High-Performance Computer Research Center, Institute of Computing Technology, Chinese Academy of Science. My advisor is Prof. [Guangming Tan](http://www.ict.cas.cn/sourcedb_2018_ict_cas/cn/jssrck/201011/t20101123_3028170.html). My research interests span computer architecture and computational biology. In particular, I'm interested in developing algorithms and domain-specific hardware accelerators that enable faster and cheaper progress in biology and medicine.