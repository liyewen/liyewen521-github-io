---
layout: post
date: 2022-10-18 07:59:00-0400
inline: true
---

One Paper accepted to HPCA 2023. NvWa: Enhancing Sequence Alignment Accelerator Throughput via Hardware Scheduling
